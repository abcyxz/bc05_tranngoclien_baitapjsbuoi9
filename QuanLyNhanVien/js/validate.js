function getFalse  (spanID, mess) {
    getEle(spanID).innerHTML = mess;
    getEle(spanID).style.display= "block";
}

function getTrue(spanID) {
    getEle(spanID).innerHTML = "";
    getEle(spanID).className = "";
    getEle(spanID).style.display= "none";
}


function xacThuc(){
    this.checkXacThuc = function(input, spanID, mess){
        if (input.trim() ===""){
            getFalse(spanID, mess);
            return false;    
        }
            getTrue(spanID);
            return true;
        
    };

    this.checkKhoangTrong = function(input, spanID, mess) {
        if(/\s/g.test(input)){
            getFalse(spanID, mess);
            return false;
        }
            getTrue(spanID);
            return true;
    };


    this.checkKiTuDacBiet = function (input, spanID, mess){
        if(/[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(input)){
            getFalse(spanID, mess);
            return false;
        }
            getTrue(spanID);
            return true;
    };

    this.checkSoKiTu = function (input, spanID, mess, min, max) {
        if (input.length >= min && input.length <= max){
            getTrue(spanID);
            return true;
        }
            getFalse(spanID, mess);
            return false;
    };

    this.checkKiTu = function (input, spanID, mess){

        var letter = "^[a-zA-Z_ÀÁÂÃÈÉÊẾÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶ" + "ẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợ" + "ụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$";
        if (input.match(letter)){
            getTrue(spanID);
            return true;
        }
            getFalse(spanID, mess);
            return false;
        
    };

    this.checkSo = function (input, spanID, mess) {
        var letter = /^[0-9]+$/;
        if (input.match(letter)){
            getTrue(spanID);
            return true;
        }
            getFalse(spanID, mess);
            return false;
        

    };

    this.checkMatKhau = function (input, spanID, mess) {
        var letter = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{0,}$/;
        if (input.match(letter)){
            getTrue(spanID);
            return true;
        }
            getFalse(spanID, mess);
            return false;
        

    };

    this.checkMail = function (input, spanID, mess){
        var letter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (input.match(letter)){
            getTrue(spanID);
            return true;
        }
            getFalse(spanID, mess);
            return false;
    };

    this.checkNgay = function (input, spanID, mess){
        // var letter = /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/;
        var letter = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/; 
        if (input.match(letter)){
            getTrue(spanID);
            return true;
        }
            getFalse(spanID, mess);
            return false;
        
    };

    this.checkChucVu = function (idSelect, spanID, mess){
        if (getEle(idSelect).selectedIndex !=0){
            getTrue(spanID);
            return true; 
        }
            getFalse(spanID, mess);
            return false;
     
    };

    this.checkTaiKhoan = function (input, spanID, mess, arr){
        var status = true;
        for (var i = 0; i < arr.length; i++){
            if (arr[i].account === input){
                status = false;
                break;
            }
        }
        if (status) {
            getTrue(spanID);
            return true;
        }                
            getFalse(spanID, mess);
            return false;
        
    }

}
